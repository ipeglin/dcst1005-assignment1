
## Declaring global variables

# Location of the daily backup files
$FragmentBackupDirectory = "$env:userprofile\Documents\Backup\Fragments"
# Todays date
$Date = (Get-Date).ToShortDateString()
# Yesterdays date
$Yesterday = (Get-Date).AddHours(-24)



## Create a new DIR for the daily backups

$NewFragmentBackup = "$FragmentBackupDirectory\Fragment $Date"
mkdir -Path $NewFragmentBackup



## Scanning through userprofile to detect recently modified files and DIRs

# Recursive function for searching through files
function DFS-Directory {
    # Default starting path is set to current working DIR
    param (
        $Path = "."
    )
    # List all the elements within the start DIR
    $DirectoryContent = Get-ChildItem -Path $Path -Exclude "*.vscode*","*Documents*" | Select-Object $_ # NB! Currently excluding documents and .vscode to prevent infinite loop
    
    # For every element in the start DIR:
    foreach ($item in $DirectoryContent) {

        # If the element is a DIR. Do this...
        if ($item.GetType().Name -like "DirectoryInfo") {
            # List all the elements withing the subDIR
            $SubdirectoryContent = Get-ChildItem -Path $item.FullName
            
            # If the subDIR is not empty. Do this...
            if ($SubdirectoryContent.count -gt 0) {

                # If the subDIR has been modified the last 24H. Do this...
                if ($item.LastWriteTime -ge $Yesterday) {
                    # Remove $env:userprofile from path
                    $Filepath = $item.FullName.Replace("$env:userprofile\", "")
                    # Declare the path to the subDIR respective backup location
                    $NewFilepath = "$NewFragmentBackup\$Filepath"
                    # Create the new DIR
                    mkdir $NewFilepath -ErrorAction SilentlyContinue
                }

                # For every subDIR pass the current path to recursively dive deeper to next level
                foreach ($Directory in $SubdirectoryContent) {
                    DFS-Directory -Path $Directory.FullName
                }
            }
        }
        # Else if it is a file. Do this...
        ElseIf ($item.GetType().Name -like "FileInfo") {

            # If the file has been modified the last 24H. Do this...
            if ($item.LastWriteTime -ge $Yesterday) {
                # Remove $env:userprofile from path
                $Filepath = $item.FullName.Replace("$env:userprofile\", "")
                # Declare the path to the subDIR respective backup location
                $NewFilepath = "$NewFragmentBackup\$Filepath"
                # Remove the filename from the NewFilepath to enable creation of the DIR. This makes it possible to copy the file to the destiniation
                $Directory = $Newfilepath.Replace($item.Name, "")
                # Create the new DIR
                mkdir "$Directory" -ErrorAction SilentlyContinue
                # Copy the item over to destination
                Copy-Item $item.FullName $NewFilepath -ErrorAction SilentlyContinue
            }
        }
    }
}



## Set all files to readonly to prevent ransomware from encrypting content

function SetReadOnly {
    # Listing all the files and DIRs that have been copied over
    $BackupFiles = Get-ChildItem -Path "$NewFragmentBackup" -Recurse

    # For every element. Do this...
    foreach ($item in $BackupFiles) {
        # Attempt setting every element to read only
        try {
            $item.IsReadOnly = $true
        }
        catch {
            continue
        }
    }
}

# Run functions
DFS-Directory -Path $env:userprofile
SetReadOnly