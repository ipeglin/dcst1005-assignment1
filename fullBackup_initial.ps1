
## Declaring global variables

# Location of the full backup
$FullBackupDirectory = "$env:userprofile\Documents\Backup\Full Backup"
# Todays date
$Date = (Get-Date).ToShortDateString()



## Create a new DIR for the first full backup

$NewFullBackup = "$FullBackupDirectory\Full Backup $Date"
mkdir -Path $NewFullBackup -ErrorAction SilentlyContinue



## Copy over all files from the $env:userprofile DIR

# Listing all the subDIRs at depth=1 from $env:userprofile. Excluding .vscode and Documents to prevent infinite loop
$UserprofileContent = Get-ChildItem -Path "$env:userprofile" -Exclude "*.vscode*","*Documents*" | Select-Object $_

# Creating identical subDIRs in the destination folder now as empty folders will not be included in seconds "foreach" loop
foreach ($dir in $UserprofileContent) {
    # Setting up for new destination path
    $DirectoryPath = $dir.FullName.Replace("$env:userprofile\", "")
    $NewDirectoryPath = "$NewFullBackup\$DirectoryPath"
    
    # If the listed element in $env:userprofile is a DIR. Do this...
    if ($dir.GetType().Name -like "DirectoryInfo") {
        # Create a new DIR in the backup DIR
        mkdir $NewDirectoryPath -ErrorAction SilentlyContinue
    }
}

# For each element in the sublevel DIRs. Do this...
foreach ($element in $UserprofileContent) {
    # Listing all subelements recursively
    $SubdirectoryContent = Get-ChildItem -Path $element.FullName -Recurse
    
    # For each of these subelements. Do this...
    foreach ($item in $SubdirectoryContent) {

        # If the element is a file. Do this...
        if ($item.GetType().Name -like "FileInfo" -And $item.FullName -notlike "*Documents*") {
            # Setting up for new destination path
            $SubdirectoryPath = $item.FullName.Replace($item.Name, "")
            $SubdirectoryPath = $SubdirectoryPath.Replace("$env:userprofile\", "")
            $Directory = "$NewFullBackup\$SubdirectoryPath"
            
            # Create a new DIR in the backup DIR
            mkdir $Directory -ErrorAction SilentlyContinue
            #Copy over the file to the new DIR created on the previous line
            Copy-Item $item.FullName $Directory
        }
        # Else if it is a DIR. Do this...
        Elseif ($item.GetType().Name -like "DirectoryInfo"  -And $item.FullName -notlike "*Documents*") {
            # Setting up for new destination path
            $DirectoryPath = $item.FullName.Replace("$env:userprofile\", "")
            $Directory = "$NewFullBackup\$DirectoryPath"

            # Create a new DIR in the backup DIR
            mkdir $Directory -ErrorAction SilentlyContinue
        }
    }
}

# Listing all the files and Dirs that have been copied over
$BackupFiles = Get-ChildItem -Path $NewFullBackup -Recurse

# For every element. Do this...
foreach ($item in $BackupFiles) {
    # Attempt setting every element to read only
    try {
        $item.IsReadOnly = $true
    }
    catch {
        continue
    }
}

# Compressing the full backup DIR to reduce file size
Compress-Archive -Path $NewFullBackup -DestinationPath "$NewFullBackup.zip" -CompressionLevel optimal
# Deletes the non-compressed backup DIR
Remove-Item -Force -Recurse -Path $NewFullBackup