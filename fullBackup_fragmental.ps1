
## Declaring global variables

# Location of the full backup
$FullBackupDirectory = "$env:userprofile\Documents\Backup\Full Backup"
# Location of the fragmented backups
$FragmentedBackupDirectory = "$env:userprofile\Documents\Backup\Fragments"
# Todays date
$Date = (Get-Date).ToShortDateString()



## Create a new DIR for the first full backup

$NewFullBackup = "$FullBackupDirectory\Full Backup $Date"
mkdir -Path $NewFullBackup -ErrorAction SilentlyContinue



## Find all the fragmented backups from the previous week

# Selecting the 7 most recent partial backups and sort them correctly
$SelectedFragments = Get-ChildItem -Path $FragmentedBackupDirectory | Sort-Object CreationTime -Descending | Select-Object -First 7 | Sort-Object CreationTime # Set this to 6 if you know there won't be any changes on sundays



## Create locations and copy files to full backup DIR

# Recursive function that dives down in the DIR tree to create new DIRs and copy files to their respective locations
function DFS-Appending {
    param (
        $Path = "."
    )
    # Listing content within each DIR
    $DirectoryContent = Get-ChildItem -Path $Path | Select-Object $_

    # For each element in the DIR. Do this...
    foreach ($element in  $DirectoryContent) {

        # If the element is a DIR. Do this...
        if($element.GetType().Name -like "DirectoryInfo") {
            # Run the function with the current path do dive deeper
            DFS-Appending -Path $element.FullName
        }
        # Else if element is a file. Do this...
        Elseif ($element.GetType().Name -like "FileInfo") {
            # Format the path to create a new path
            $RelativePath = $element.FullName.Replace($FragmentedBackupDirectory, "").Replace($element.Name, "").Split("\", 3)[2]
            # Declaring new location path
            $NewPath = "$NewFullBackup\$RelativePath"
            $NewPath = $NewPath.Replace("\\", "\")

            # If the path is longer than zero, AKA is normally stored directly in $env:userprofile
            if ($RelativePath.Length -gt 0) {
                # Create a new DIR in this location
                mkdir $NewPath -ErrorAction SilentlyContinue
                # Copy over the element into the DIR made on previous line
                Copy-Item $element.FullName $NewPath -ErrorAction SilentlyContinue
            }
            # Else. Do this...
            else {
                # Copy over file to the new location since the DIR allready exists
                Copy-Item $element.FullName $NewPath -ErrorAction SilentlyContinue
            }
            # Unda Read-Only for all files recursively to make sure newer versions from later fragments can overwrite previous versions
            RemoveReadOnly -Path $NewFullBackup
        }
    }
}

# Function that removes Read-Only recursively from given path
function RemoveReadOnly {
    param (
        $Path = "."
    )
    # Listing content DIR
    $Content = Get-ChildItem -Path $Path -Recurse

    # For each element. Do this...
    foreach ($element in $Content) {
        try {
            $element.IsReadOnly = $false
        }
        catch {
            continue
        }
    }
}

# Function that sets all files back to Read-Only
function SetReadOnly {
    # Listing content in DIR
    $BackupFiles = Get-ChildItem -Path $NewFullBackup -Recurse

    # For each element. Do this...
    foreach ($element in $BackupFiles) {
        try {
            $element.IsReadOnly = $true
        }
        catch {
            continue
        }
    }
}

# Running recursive function for all the fragmented backup DIRs
foreach ($fragment in  $SelectedFragments) {
    DFS-Appending -Path $fragment.FullName
}
# Setting all files to Read-Only
SetReadOnly

# Compressing the full backup DIR to reduce file size
Compress-Archive -Path $NewFullBackup -DestinationPath "$NewFullBackup.zip" -CompressionLevel optimal
# Deletes the non-compressed backup DIR
Remove-Item -Force -Recurse -Path $NewFullBackup