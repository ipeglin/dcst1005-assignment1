# DCST1005: Ransomware Protection

GitLab repository: [https://gitlab.com/ipeglin/dcst1005-assignment1](https://gitlab.com/ipeglin/dcst1005-assignment1)

## Table of Content

[[_TOC_]]


## 1 Introduction
Ransomware is a form of malware that is designed to hold a person's files hostage to gain value, most often in the form of money. With this method of extortion, the victims' files are typically encrypted or locked which makes them inaccessible or unusable to the owner. The aim is to make the victim pay a ransom fee that will unlock or decrypt the files. The problem lies with how one can protect oneself from this type of threat, as it is almost impossible to prevent. Arguably the best option is to regularly take a backup of your machine's status and store this data somewhere the malware cannot reach it. This can be quite crucial as some malware are worms, which means that it will try to spread to other systems via your computer. One example of this is the (in)famously known WannaCry worm, which spread to hundreds of thousands of computers, encrypting an immense amount of important files worldwide.

In this assignment, students have been tasked with developing a method of threat "prevention" (using the term loosely) in the form of a backup using [Windows Powershell](https://docs.microsoft.com/en-us/powershell/). The assignment was quite open, and students could freely choose their preferred technique. For instance; If one wanted to grab files from a client and store it on a server by running the script on a domain controller. For doing this, domain file-sharing or PSSession were proposed, even though the lessons this far have not dug deep into these two technologies.



##  2 Methods and Design

### 2.1 Backup Type
For this assignment, the chosen method of backup was an incremental backup. The reason for this choice of backup type was to only back up necessary files as this prevents inefficient use of storage. Another result of this would also be that the network load would be significantly reduced as every computer within the network will not send full backups at the same time every night. This means in practice that for the most part that six out of seven backup directories every week will be significantly smaller in the number of bytes in comparison to the full backup.


### 2.2 Script Structure

#### 2.2.1 Backup Controller
The idea is to take advantage of the task scheduler to run a backup controller. This controller will be run every day at night time and run a script based on which weekday it is. If it turns out to be any other day than Sunday, the program is set to run only an incremental backup. There will be a separate script for both incremental- and full backup. This makes it more structured and easier to modify the functionalities.

#### 2.2.2 Full Backup: [fullBackup_initial.ps1](https://gitlab.com/ipeglin/dcst1005-assignment1/-/blob/master/fullBackup_initial.ps1)
The purpose of this script is simply to make a complete mirrored copy of the user profile directory as a reference to when the backups started. This script is needed as the method of incremental backup relies on a previous full backup. At the moment the first full backup is in place, the incremental backups from there on out will only include what has been changed during the last 24 hours. This is the data that will make up the next full backup.

#### 2.2.3 Incremental Backup: [incrementalBackup.ps1](https://gitlab.com/ipeglin/dcst1005-assignment1/-/blob/master/incrementalBackup.ps1)
The incremental backup script will perform a depth-first search in the users' directories using a recursive function. If there is a file in the directory AND it was modified within the last 24 hours, the file will be copied to the backup under "Fragment [BackupDate]". To make sure that the Copy-Item command will not fail because of a missing destination, the program will try to make a new directory with the files' respective parent path. However, if the program encounters a directory, it will check if it is not empty and if it was recently modified, if the will be created a new directory in the backup. Next, the function will recurse for each of the directories. After recursing through the entirety of the user profile and copying over necessary files, the script will run a function that sets all the files to Read-Only.

#### 2.2.4 Full Backup: [fullBackup_fragmental.ps1](https://gitlab.com/ipeglin/dcst1005-assignment1/-/blob/master/fullBackup_fragmental.ps1)
The fragmental full backup script is the script that stitched all the fragmented backups from the previous week together. To make sure that the most recently changed file will be prioritized, the script copies the files in chronological order. This is followed by setting every file to IsRead-Only = $false, and each file can be overwritten if there is an updated version. The script will go through all the partial backups and continually overwrite new changes to files or just copy over files that have not been backed up previously. After all the elements have been copied over, the program will once again list all the elements recursively and set each element back to Read-Only.



## 3 Discussion

### 3.1 Problem Solution
As the functionality of the backup has been divided into several smaller scripts some problems might occur. When the day comes for a full backup to be performed, an incremental backup will first be executed to include the changes the might have been made during Sunday. Since these programs are separate, they might also execute asynchronously, this means that the script has the potential of starting to run the fullBackup_fragmental.ps1 script before the incrementalBackup.ps1 script is finished. This may result in a faulty backup where not all the necessary files are included. It is also a possibility that older files from earlier incremental backups might "bleed" into the new full backup. A workaround could be to delete all the fragmented backup directories when the script is done copying the files since the full backup only tried to obtain the seven most recently added directories.

<img src="Images/DCST1005_ Mappeoppgave1_UML_Large.png">


### 3.2 Security
As a goal to prevent files from being encrypted these scripts have a couple of flaws as of now. Since the backups have been sent to a local directory, there is a possibility that the files themselves could be encrypted and inaccessible, and not the contents. Since I had trouble connecting to the client VM with PSSession and transfer the files using this technology, the scripts do not include this feature. If I were to improve these scripts in the future, I would make sure that the scripts, scan through the client computers' file and copies them over to the domain controller itself, or preferably a dedicated server. Since the scripts are currently storing the backups locally, I attempted to at least prevent modification of the files' content. To do this I set all the files in the backup to Read-Only, in which one must create a new file by another name to save changes to it.



## 4 Conclusion
In conclusion, the scripts work but should be further developed to store the backups to a remote server. This solution is also a bit more sparse with respect to storage space as the full backups are compressed, and only contain the files that have been modified. With exception to the initial full backup, there are not empty directories either.



## 5 Sources

* [Copying directories recursively](https://www.spguides.com/powershell-global-variable/)
* [Counting content within a directory](https://stackoverflow.com/questions/5111572/how-to-test-for-null-array-in-powershell)
* [Depth-first search](https://en.wikipedia.org/wiki/Depth-first_search)
* [Finding file location of the current running script](https://stackoverflow.com/questions/5466329/whats-the-best-way-to-determine-the-location-of-the-current-powershell-script)
* [Functions in PowerShell](https://docs.microsoft.com/en-us/powershell/scripting/learn/ps101/09-functions?view=powershell-7.1)
* [Getting dates and time](https://devblogs.microsoft.com/scripting/adding-and-subtracting-dates-with-powershell/)
* [How to make global changes to a variable](https://www.spguides.com/powershell-global-variable/)
* [Listing all files in a directory](https://stackoverflow.com/questions/65894132/ps-to-list-specific-directories-that-contain-files)
* [Making arrays that can be modified](https://www.jonathanmedd.net/2014/01/adding-and-removing-items-from-a-powershell-array.html)
* [Passing arguments into functions](https://www.youtube.com/watch?v=RW2tRebxqzQ)
* [Recursively delete a directory](https://stackoverflow.com/questions/38141528/cannot-remove-item-the-directory-is-not-empty)
* [Replacing portions of a string](https://www.itprotoday.com/powershell/remove-string-string-powershell)
* [Selecting recently modified files and directories](https://stackoverflow.com/questions/19774097/finding-modified-date-of-a-file-folder)
* [Setting a scheduled task](https://stackoverflow.com/questions/53476875/run-scripts-at-a-specific-time-with-powershell)
* [Splitting a string at a specific occurrence](https://stackoverflow.com/questions/25383263/powershell-split-a-string-on-first-occurrence-of-substring-character)
* [Sorting folder respective to LastWriteTime 2](https://stackoverflow.com/questions/9675658/powershell-get-childitem-most-recent-file-in-directory)
* [Sorting folder respective to LastWriteTime](https://spiderip.com/blog/2018/06/powershell-script-to-get-latest-changed-files-from-a-folder)
* [Using for loops in ps1](https://stackoverflow.com/questions/27851292/run-all-powershell-scripts-in-a-directory)
* [Zipping a directory](https://4sysops.com/archives/zip-and-unzip-with-powershell/)