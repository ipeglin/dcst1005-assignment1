# Function that sets a regular task to be run every day at 21:00. The script will only be run once
function setTaskSchedule {
    # Setting a standard time to 21:00 if no parameters are passed
    param (
        $ScheduledTime = "21:00"
    )
    $TaskName = "Run backupController"
    $User = "skrrt.sec\Administrator"
    $ScriptPath = "$PSScriptRoot\backupController.ps1"

    $Trigger = New-ScheduledTaskTrigger -At $ScheduledTime -Daily
    $Action = New-ScheduledTaskAction -Execute "pwsh.exe" -Argument "$ScriptPath"
    Register-ScheduledTask -TaskName $TaskName -Trigger $Trigger -User $User -Action $Action -RunLevel Highest -Force
}

setTaskSchedule